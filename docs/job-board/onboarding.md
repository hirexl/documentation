---
sidebar_position: 2
---

# Onboarding

Welcome to the HireXL Job Seeker Onboarding!

At HireXL, we're committed to helping you find your dream job and advance your career. Our onboarding process is designed to ensure that you have a seamless and rewarding experience from the moment you join our platform.

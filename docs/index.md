---
sidebar_position: 1
---

# Overview

HireXL's ATS Job Board is a comprehensive platform designed to streamline the recruitment process for businesses of all sizes. Tailored to meet the needs of modern hiring practices, it offers robust features to efficiently manage job postings, applicant tracking, and candidate communication.

Let's discover **HireXL Services and Architecture**.

### Key Features:

1. **User-friendly Interface:** The ATS Job Board provides an intuitive interface that simplifies job posting and candidate management tasks, ensuring a seamless user experience for recruiters and hiring managers.

2. **Customizable Job Listings:** Employers can create and customize job listings with rich media content, including videos and images, to attract top talent and effectively communicate their employer brand.

3. **Advanced Candidate Filtering:** With powerful filtering options, recruiters can quickly identify qualified candidates based on specific criteria, such as skills, experience, and location, helping to streamline the selection process.

4. **Collaborative Hiring Tools:** HireXL's ATS Job Board facilitates collaboration among hiring teams with features such as candidate scoring, feedback collection, and interview scheduling, ensuring a coordinated and efficient hiring process.

5. **Integration Capabilities:** The platform seamlessly integrates with other HR tools and systems, including HRIS (Human Resources Information Systems) and third-party job boards, providing a unified recruitment solution.

6. **Analytics and Reporting:** Recruiters can leverage analytics and reporting tools to gain insights into recruitment performance, track key metrics, and make data-driven decisions to optimize hiring strategies.

Overall, HireXL's ATS Job Board empowers organizations to attract, engage, and hire top talent effectively, streamlining the recruitment lifecycle and driving business success.

### Technology Requirements

To run your app smoothly, ensure that you have the following technology stack properly set up:

- [x] [Node.js](https://nodejs.org/en/download/) Version 10.14 or above is required. When installing Node.js, make sure to check all checkboxes related to dependencies for optimal performance.
- [x] [Reactjs](https://reactjs.org/) Version 16.12.0 or above is recommended to leverage the latest features and improvements.
- [x] [Nextjs](https://nextjs.org/) This framework is essential for server-side rendering and creating dynamic web applications.
- [x] [TypeScript](https://www.typescriptlang.org/) Embrace strong typing and improved tooling by incorporating TypeScript into your development workflow.
- [x] [Nestjs](https://nestjs.com/) A progressive Node.js framework for building efficient, reliable and scalable server-side applications.
- [x] [PostgreSQL](https://www.postgresql.org/) Ensure you have PostgreSQL installed and configured as your app's primary database for efficient data management.
- [x] [RabbitMQ](https://www.rabbitmq.com/) Incorporate RabbitMQ for message queuing and task scheduling to enhance the scalability and reliability of your app.
- [x] [Amazon SES](https://aws.amazon.com/ses/) Leverage Amazon SES for reliable and cost-effective email delivery, ensuring seamless communication with your users.
- [x] [Amazon SNS](https://aws.amazon.com/sns/) Utilize Amazon SNS for push notifications, SMS messaging, and other real-time communication needs within your app.
- [x] [Docker](https://www.docker.com/) Dockerize your app for easy deployment and management across different environments, ensuring consistency and reliability in your deployment process.
- [x] [GitLab CI](https://gitlab.com/) Implement GitLab CI for continuous integration and automated testing, streamlining your development workflow and ensuring code quality.

By meeting these technology requirements, you can ensure that your app runs efficiently and effectively, providing a seamless experience for both users and developers.

## Systems Overview: HireXL Job Board and ATS

### Job Board

The Jobseeker Portal offers essential functionalities for job seekers: https://hirexl.in/

- **Account Management**: Job seekers can easily log in or sign up and create their profiles to showcase their qualifications and experience.

- **Job Search and Application**: Users can search for available job listings and apply for positions that match their skills and interests.

- **Follow-up Support**: Job seekers receive follow-ups from the Savanna HR recruitment team, who may suggest multiple job opportunities based on their profiles, providing personalized assistance throughout the job search process.

### Recruiter ATS

The Recruiter Portal, an Isolated Multi-tenant System accessible via a unique URL (e.g., [https://recruiters.hirexl.in/](https://recruiters.hirexl.in/)), offers a suite of functionalities tailored for recruiters:

- **Team Collaboration**: Recruiters can easily invite other team members to the job board/portal, facilitating seamless collaboration in the recruitment process.

- **Job Listing Management**: Recruiters have the ability to create job listings directly within the portal, ensuring that job openings are promptly showcased on the Jobseeker Portal for maximum visibility.

- **Application Tracking**: Recruiters can efficiently track and manage job applications received against posted job listings, streamlining the candidate selection process.

- **CV Upload**: Recruiters can upload candidate CV(s) directly to the system, making it convenient to manage applicant documents and match them with relevant job openings.

By leveraging the functionalities of both the Jobseeker Portal and the Recruiter Portal, HireXL offers a comprehensive platform that caters to the needs of both job seekers and recruiters, facilitating a seamless and efficient recruitment process.

---
sidebar_position: 1
---

# Login & Registration

Login & registration collection has all the API(s) those are required to UI service to completion.

It help user to login, registration, reset forgot password

[![Run in Postman](https://run.pstmn.io/button.svg)](https://app.getpostman.com/run-collection/97189-ad52df53-7323-4e98-882c-5d1c3678b4c1?action=collection%2Ffork&collection-url=entityId%3D97189-ad52df53-7323-4e98-882c-5d1c3678b4c1%26entityType%3Dcollection%26workspaceId%3D0c114c87-942e-4c95-af76-e2fb50d326f4)

- [Login](https://www.postman.com/asb14690/workspace/a4abs/request/97189-6d230b41-090e-4654-ab5f-56fe2392eb8c)
- [Social Login](https://www.postman.com/asb14690/workspace/a4abs/request/97189-6070c598-55ed-4c8d-98f9-f01c40091fea)
- [Facebook Auth](https://www.postman.com/asb14690/workspace/a4abs/request/97189-2d8acaa6-de85-43a7-9730-468d9ad3d84a)
- [Google Auth](https://www.postman.com/asb14690/workspace/a4abs/request/97189-c0d451ed-7b64-44c0-995e-13254482ddb7)
- [Linkedin Auth](https://www.postman.com/asb14690/workspace/a4abs/request/97189-c5384ba4-f371-4d77-9276-d8933e42c049)
- [Register User with Resume](https://www.postman.com/asb14690/workspace/a4abs/request/97189-f78fba18-7e88-4aeb-9ba9-38da6a3bd3e2)
- [Send OTP to Reset Password](https://www.postman.com/asb14690/workspace/a4abs/request/97189-c0dcc4bb-cf84-4380-a1d7-8df4c11a4827)
- [Reset Password with OTP](https://www.postman.com/asb14690/workspace/a4abs/request/97189-ff7687d5-791a-45fa-827e-ad0a1ff66e1d)

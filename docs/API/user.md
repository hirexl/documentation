---
sidebar_position: 3
---

# User Profile

To manage user profile and user jobs applications. we need to integrate to user profile related backend API.

It help user manage their profile and personalized jobs application status

[![Run in Postman](https://run.pstmn.io/button.svg)](https://app.getpostman.com/run-collection/97189-ad52df53-7323-4e98-882c-5d1c3678b4c1?action=collection%2Ffork&collection-url=entityId%3D97189-ad52df53-7323-4e98-882c-5d1c3678b4c1%26entityType%3Dcollection%26workspaceId%3D0c114c87-942e-4c95-af76-e2fb50d326f4)

- [Fetch Personal Information](https://www.postman.com/asb14690/workspace/a4abs/request/97189-5c57fb86-e27d-4414-8658-ebcd0fdb1558)
- [Add User info](https://www.postman.com/asb14690/workspace/a4abs/request/97189-7098541e-6aa8-49b2-99b8-2fb461bc3dc5)
- [Add About User](https://www.postman.com/asb14690/workspace/a4abs/request/97189-38f25f8b-0cea-45e5-98b1-f6ef01a42813)
- [Add Education](https://www.postman.com/asb14690/workspace/a4abs/request/97189-3d009827-fd91-4722-84d2-222306a58122)
- [Delete Education](https://www.postman.com/asb14690/workspace/a4abs/request/97189-821197e3-8836-44e8-af8e-acbfb1bb16af)
- [Update Education](https://www.postman.com/asb14690/workspace/a4abs/request/97189-459f6056-4943-455e-9ea5-bf34d8f0f7f9)
- [Delete Experience](https://www.postman.com/asb14690/workspace/a4abs/request/97189-d3a846ae-f9fc-4490-b6ba-77acc739eaac)
- [Update Experience](https://www.postman.com/asb14690/workspace/a4abs/request/97189-c32d774a-962f-491f-a3bb-8b4bfa593a3d)
- [Experience](https://www.postman.com/asb14690/workspace/a4abs/request/97189-c315a40f-ff14-4e1d-b0f9-f750a09f8d28)
- [Upload User Image](https://www.postman.com/asb14690/workspace/a4abs/request/97189-6cea80bc-5b8a-4699-a258-4fe7620ab40c)
- [Add User Skills](https://www.postman.com/asb14690/workspace/a4abs/request/97189-b502d823-d3dc-4d4a-9da7-cb402d443c6d)
- [Add User Achievement](https://www.postman.com/asb14690/workspace/a4abs/request/97189-11b4e649-0d9e-4904-ba53-64c462ee08f4)
- [Change Password](https://www.postman.com/asb14690/workspace/a4abs/request/97189-ee04746a-d5a0-4764-bdba-f8dd73771e7d)
- [Get User Experience](https://www.postman.com/asb14690/workspace/a4abs/request/97189-2c98352e-0bf5-4367-86f9-bdeef21f0721)
- [Get User Resume](https://www.postman.com/asb14690/workspace/a4abs/request/97189-cb5e7fe5-5aee-48ef-a882-a17cd1fc5787)
- [Get User Image](https://www.postman.com/asb14690/workspace/a4abs/request/97189-168d47ec-152d-4089-8cf2-847afa2d7339)
- [Get Job Applications](https://www.postman.com/asb14690/workspace/a4abs/request/97189-6ae9097e-3e7c-4b69-99d9-0836a1d9b8dd)

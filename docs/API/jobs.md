---
sidebar_position: 2
---

# Job & Filters

To integrate Jobs listing, filters, search, and other job posting related functionalities we need to use following API(s).

It help user search a job to match their preferences and apply to the job

[![Run in Postman](https://run.pstmn.io/button.svg)](https://app.getpostman.com/run-collection/97189-ad52df53-7323-4e98-882c-5d1c3678b4c1?action=collection%2Ffork&collection-url=entityId%3D97189-ad52df53-7323-4e98-882c-5d1c3678b4c1%26entityType%3Dcollection%26workspaceId%3D0c114c87-942e-4c95-af76-e2fb50d326f4)

- [Resent Job](https://www.postman.com/asb14690/workspace/a4abs/request/97189-8e0db08b-4123-44e1-9af6-d417ba823451)
- [Get Job Details](https://www.postman.com/asb14690/workspace/a4abs/request/97189-5d2d798e-8bde-4eac-9534-687ecb099257)
- [Job Search](https://www.postman.com/asb14690/workspace/a4abs/request/97189-448ef577-1db3-4688-ac74-0a9fd594c052)
- [Functional Area](https://www.postman.com/asb14690/workspace/a4abs/request/97189-a0f3ae91-c96b-45d5-b194-3cec71e5277b)
- [Get Cities](https://www.postman.com/asb14690/workspace/a4abs/request/97189-f8b0b128-50cd-4427-997d-933d183eb7ac)
- [Sitemap](https://www.postman.com/asb14690/workspace/a4abs/request/97189-562e9bfa-4de1-495e-858b-2c3ece3e5fd0)
- [Job Applied Status](https://www.postman.com/asb14690/workspace/a4abs/request/97189-754cafeb-d883-47cd-8214-e17112d7c86d)
- [Functional Area filter](https://www.postman.com/asb14690/workspace/a4abs/request/97189-e7e4d31f-9073-41c0-88a8-fb355ae2e7aa)
- [Job Filters](https://www.postman.com/asb14690/workspace/a4abs/request/97189-5d837771-7dd9-45b5-b4d1-f25807bf4980)
- [Apply Filters](https://www.postman.com/asb14690/workspace/a4abs/request/97189-ec265113-6e88-469a-b321-56f297f75c44)
